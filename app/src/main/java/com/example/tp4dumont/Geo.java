package com.example.tp4dumont;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class Geo extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_geo);
    }

    public void onClick(View view){
        if(view.getId() == R.id.valider){
            EditText latitude = findViewById(R.id.latitude);
            EditText longitude = findViewById(R.id.longitude);
            if (longitude.getText().length() == 0 || latitude.getText().length() == 0){
                Toast.makeText(getApplicationContext(), R.string.error_geo, Toast.LENGTH_SHORT).show();
            }
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(android.net.Uri.parse("geo:" + latitude.getText().toString() + "," + longitude.getText().toString()));
            try {
                startActivity(intent);
            }
            catch (Exception e) {
                Toast.makeText(getApplicationContext(), R.string.error, Toast.LENGTH_SHORT).show();
            }
        }
        else {
            finish();
        }
    }
}