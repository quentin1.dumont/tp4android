package com.example.tp4dumont;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class Web extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);
    }

    public void onClick(View view) {
        if (view.getId() == R.id.annuler) {
            finish();
        } else {
            Intent intent = new android.content.Intent();
            intent.setAction(Intent.ACTION_VIEW);
            EditText editText = findViewById(R.id.url);
            intent.setData(android.net.Uri.parse(editText.getText().toString()));
            try {
                startActivity(intent);
            }
            catch (Exception e) {
                Toast.makeText(getApplicationContext(), R.string.error, Toast.LENGTH_SHORT).show();
            }
        }
    }
}