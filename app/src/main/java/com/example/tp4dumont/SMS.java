package com.example.tp4dumont;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class SMS extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms);
    }

    public void onClick(View view) {
        if (view.getId() == R.id.valider) {
            EditText numero = findViewById(R.id.numero);
            if (numero.getText().toString().length() == 10 && !numero.getText().toString().contains(".")) {
                EditText message = findViewById(R.id.msg);
                if (message.getText().length() != 0) {
                    android.content.Intent intent = new android.content.Intent();
                    intent.setAction(Intent.ACTION_SENDTO);
                    intent.setData(android.net.Uri.parse("sms:" + numero.getText().toString()));
                    intent.putExtra("sms_body", message.getText().toString());
                    try {
                        startActivity(intent);
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), R.string.error, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), R.string.error_message, Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getApplicationContext(), R.string.error_number, Toast.LENGTH_SHORT).show();
            }
        } else if (view.getId() == R.id.annuler) {
            finish();
        }
    }
}