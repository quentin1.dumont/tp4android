package com.example.tp4dumont;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class Call extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call);
    }

    public void onClick(View view){
        if(view.getId() == R.id.valider){
            EditText editText = findViewById(R.id.numero);
            if (editText.getText().toString().length() == 10 && !editText.getText().toString().contains(".")) {
                android.content.Intent intent = new android.content.Intent();
                intent.setAction(android.content.Intent.ACTION_DIAL);
                intent.setData(android.net.Uri.parse("tel:" + editText.getText().toString()));
                try {
                    startActivity(intent);
                }
                catch (Exception e) {
                    Toast.makeText(getApplicationContext(), R.string.error, Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getApplicationContext(), R.string.error_number, Toast.LENGTH_SHORT).show();
            }
        }
        else if(view.getId() == R.id.annuler){
            finish();
        }

    }
}