package com.example.tp4dumont;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClick(View view) {
        if (view.getId() == R.id.btn1){
            startActivity(new Intent(this, Call.class));
        }
        else if (view.getId() == R.id.btn2 || view.getId() == R.id.btn3) {
            startActivity(new Intent(this, SMS.class));
        }
        else if (view.getId() == R.id.btn4){
            startActivity(new Intent(this, Web.class));
        }
        else if (view.getId() == R.id.btn5){
            startActivity(new Intent(this, Geo.class));
        }
    }
}